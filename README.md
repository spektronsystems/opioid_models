# Opioid Models

This repository contains data and results on predictive modeling efforts for binding on three opioid receptors; delta, kappa, mu.

As part of a grant proposal to the National Institute of Drug Abuse (NIDA), Spektron Systems developed predictive models of three opioid receptor binding using publicly available data.  Using data for binding affinity for three opioid receptors (delta, kappa, and mu), three classification models were developed using Partial Least Squares – Discriminant Analysis.  All models produced, on average, a balanced accuracy of 0.85.  These preliminary modeling efforts show strong predictive capability for these receptors using a 3D-SDAR encoding and supervised learning methods.  Such predictive capability could play a valuable role in in silico virtual screening for hit identification.

# Project Structure
The root directory contains this file, the original data files from BindingDB (as .sdf), and a demo python script.  Subdirectories delta, kappa, and mu contain training reports from the preliminary models built using the Q-MAP platform.  Each receptor directory contains a ModelArchive directory.  These contain all of the data used to build the model.  Mol files are separated into TrainMols and TestMols, 3D-SDAR features can be found in trainFeatures and testFeatures.  These contain all of the data used to build the model.  Mol files are separated into TrainMols and TestMols. 3D-SDAR features can be found in TrainFeatures.csv and TestFeatures.csv and labels can be found in TrainLabels.csv and TestLabels.csv.

The demo script will read the 3D-SDAR features and labels files and train a simple 1-Nearest Neighbor classifier.  The script is written for Python 3.x and requires Pandas and scikit-learn.  The following statement can be used, in any ModelArchive directory, to demo the descriptors.

```python ../../sdarDescriptorDemo.py -featureFileTrain TrainFeatures.csv -labelFileTrain TrainLabels.csv -featureFileTest TestFeatures.csv -labelFileTest TestLabels.csv -outFile spektron_analysis.txt -idColumn="Molecule Name" -activityColumn="True Class"```

A note about molecule names: Spektron uses the InChIKey of a molecule as a quick, resolvable identifier.  Each row in the files is labeled using the scheme.  A mapping file from SMILES to InChIKey is provided.

# Data Source
Data used in this modeling was obtained from BindingDB. http://www.bindingdb.org

"BindingDB is a public, web-accessible database of measured binding affinities, focusing chiefly on the interactions of protein considered to be drug-targets with small, drug-like molecules. BindingDB contains 1,632,174 binding data, for 7,257 protein targets and 725,741 small molecules."

M. K. Gilson, T. Liu, M. Baitaluk, G. Nicola, L. Hwang, and J. Chong, “BindingDB in 2015: A public database for medicinal chemistry, computational chemistry and systems pharmacology,” Nucleic Acids Res., vol. 44, no. D1, pp. D1045–D1053, 2016.

# References

[1]	M. K. Gilson, T. Liu, M. Baitaluk, G. Nicola, L. Hwang, and J. Chong, “BindingDB in 2015: A public database for medicinal chemistry, computational chemistry and systems pharmacology,” Nucleic Acids Res., vol. 44, no. D1, pp. D1045–D1053, 2016.
[2]	Slavov, Svetoslav H., Elizabeth L. Geesaman, Bruce A. Pearce, Laura K. Schnackenberg, Dan A. Buzatu, Jon G. Wilkes, and Richard D. Beger. 2012. “13C NMR-Distance Matrix Descriptors: Optimal Abstract 3D Space Granularity for Predicting Estrogen Binding.” Journal of Chemical Information and Modeling 52 (7): 1854–64.
[3]	S. Farag et al., “CERAPP: Collaborative Estrogen Receptor Activity Prediction Project,” Environ. Health Perspect., vol. 124, no. 7, pp. 1023–1033, 2016.
[3]	S. H. Slavov, I. Stoyanova-Slavova, W. Mattes, R. D. Beger, and B. J. Brüschweiler, “Computational identification of structural factors affecting the mutagenic potential of aromatic amines: study design and experimental validation,” Arch. Toxicol., vol. 92, no. 7, pp. 2369–2384, 2018.
[4]	I. B. Stoyanova-Slavova, S. H. Slavov, D. A. Buzatu, R. D. Beger, and J. G. Wilkes, “3D-SDAR modeling of hERG potassium channel affinity: A case study in model design and toxicophore identification,” J. Mol. Graph. Model., vol. 72, pp. 246–255, 2017.
[5]	R. D. Beger, D. A. Buzatu, and J. G. Wilkes, “Combining NMR Spectral Information with Associated Structural Features to Form Computationally Nonintensive, Rugged, and Objective Models of Biological Activity,” in Drug Discovery Handbook, S. C. Gad, Ed. Hoboken, NJ, USA: John Wiley & Sons, Inc., 2005, pp. 227–286.
