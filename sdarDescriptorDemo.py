#!/usr/bin/env python
#
# Copyright 2019, Spektron Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import argparse

import pandas as pd
from sklearn.metrics import roc_curve, auc, confusion_matrix
from sklearn import neighbors


def gen_eval(train_data, train_labels, test_data, test_labels, method="knn1"):
    k = int(method[3:])
    classifier = neighbors.KNeighborsClassifier(k, metric=args.metric, algorithm='brute')
    classifier.fit(train_data, train_labels)
    probs = classifier.predict_proba(test_data)[:, 1]
    
    fpr, tpr, thresholds = roc_curve(test_labels, probs)
    roc_auc = auc(fpr, tpr)
    pred_labels = list(map(lambda x: int(x), probs))
    cm = confusion_matrix(test_labels, pred_labels)
    return roc_auc, cm


parser = argparse.ArgumentParser(description='')
parser.add_argument('-featureFileTrain', required=True)
parser.add_argument('-labelFileTrain', required=True)
parser.add_argument('-featureFileTest', required=True)
parser.add_argument('-labelFileTest', required=True)
parser.add_argument('-activityColumn', default="activity")
parser.add_argument('-outFile', required=True)
parser.add_argument('-metric', type=str, default='jaccard', choices=['jaccard', 'dice', 'euclidean'])
parser.add_argument('-idColumn', default='inchikey')
args = parser.parse_args()

print("Reading descriptors")
trainFeatures = pd.read_csv(args.featureFileTrain, index_col=args.idColumn)
testFeatures = pd.read_csv(args.featureFileTest, index_col=args.idColumn)

print("Reading labels")
trainLabels = pd.read_csv(args.labelFileTrain, index_col=args.idColumn)
testLabels = pd.read_csv(args.labelFileTest, index_col=args.idColumn)

train_data = trainFeatures.values
test_data = testFeatures.values
train_labels = trainLabels[args.activityColumn].tolist()
test_labels = testLabels[args.activityColumn].tolist()

if len(train_data) != len(train_labels):
    print("Training data sizes do not match")
    exit()
if len(test_data) != len(test_labels):
    print("Test data sizes do not match")
    exit()

print("Training a 1-Nearest Neighbor classifier")
knn_auc, knn_cm = gen_eval(train_data, train_labels, test_data, test_labels, method="knn1")

outFH = open(args.outFile, 'w')

print("knn3=", knn_auc,"\n",knn_cm,"\n",file=outFH)

outFH.close()

